Servo Driver 
============

A simple test to see how hard it is to write a servo driver using an 8-bit timer/counter on the AtTiny85

* Using CTC mode
* 8-bit Timer 1
* Default fuses running internal 8Mhz clock with DIV 8 
* Works only with PB1 PB2 PB3

Research

* [AVR Freaks Forum](http://www.avrfreaks.net/forum/servo-control-8-bit-timer)